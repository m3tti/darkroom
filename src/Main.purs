module Main where

import Prelude
import Effect
import Effect.Aff (Aff, launchAff)
import Effect.Class (liftEffect)
import Effect.Console (log)
import Caman (Layer(..), LayerOption(..), Processor(..), Channels(..), Filter(..), render, register)

main :: Effect Unit
main = do
  _ <- launchAff $ do
    _ <- liftEffect $ register $ Kernel { name: "test"
                                        , matrix: [ 5, 5, 5
                                                  , 5, 5, 5
                                                  , 5, 5, 5
                                                  ] 
                                        }
    _ <- render $ Main { elementId: "#lena" 
                       , filters: [ CustomFilter "test" ]
                       }
    liftEffect $ log "Done" 
  pure unit
