PROJECT=darkroom

install: 
	psc-package install

build:
	psc-package build
	echo "module.exports = { Main: require('./output/Main/index') };" > tmp.js
	browserify tmp.js --s PS -o ${PROJECT}.js
	rm tmp.js

uglify: build
	terser jslibs/* ${PROJECT}.js -o fullapp.js

serve: uglify
	python -m SimpleHTTPServer

deps: 
	psc-package dependencies

